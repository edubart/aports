# Contributor: solidnerd <niclas@mietz.io>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=glab
pkgver=1.39.0
pkgrel=0
pkgdesc="Open source GitLab CLI tool written in Go"
url="https://gitlab.com/gitlab-org/cli"
arch="all"
license="MIT"
depends="git"
makedepends="go"
options="!check"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://gitlab.com/gitlab-org/cli/-/archive/v$pkgver/cli-v$pkgver.tar.gz"
builddir="$srcdir/cli-v$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	# date seems a little broken to override
	go build -ldflags "
		-X main.debugMode=false
		-X main.version=v$pkgver
		-X main.buildDate=$(date -u "+%Y-%m-%d" ${SOURCE_DATE_EPOCH:+-d @$SOURCE_DATE_EPOCH})
		-extldflags \"$LDFLAGS\"
		" \
		-o bin/glab \
		./cmd/glab/main.go

	# XXX: When glab is run in fakeroot it segfaults for some reason
	# on ppc64le. By generating the compilation files here we
	# workaround that but we need to investigate why it segfaults in
	# fakeroot eventually.
	bin/glab completion --shell bash > bash.comp
	bin/glab completion --shell zsh > zsh.comp
	bin/glab completion --shell fish > fish.comp
}

package() {
	install -Dm755 bin/glab -t "$pkgdir"/usr/bin/

	install -Dm644 bash.comp "$pkgdir"/usr/share/bash-completion/completions/glab.bash
	install -Dm644 zsh.comp "$pkgdir"/usr/share/zsh/site-functions/_glab
	install -Dm644 fish.comp "$pkgdir"/usr/share/fish/vendor_completions.d/glab.fish
}

sha512sums="
b73e213e4ed8d2bfe4174e5dc3d8ac35d6477060eb0c7a3a9a7b5b27bc5fc4fa6993a3d568c605c8f557db44c3f087198b19827795071388341925c2ec8d6338  glab-1.39.0.tar.gz
"
